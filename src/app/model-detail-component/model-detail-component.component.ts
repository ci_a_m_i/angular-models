import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ModelsService } from '../models.service';
import { Model } from '../model.interface';
import { convertToRoman } from '../utils';

@Component({
  selector: 'app-model-detail-component',
  templateUrl: './model-detail-component.component.html',
  styleUrls: ['./model-detail-component.component.sass']
})
export class ModelDetailComponentComponent implements OnInit {
  models: Model[] = [];
  selectedModelIndex = 0;
  selectedModel: Model;
  private selectedButtonHoverStyle: { 'background': '#000' };

  constructor(private modelsService: ModelsService) { }

  ngOnInit() {
    this.models = this.modelsService.modelsList();
    this.selectedModel = this.models[this.selectedModelIndex];

    this.models = this.models.map((model, index) => {
      model.$indexRoman = convertToRoman(index + 1);
      model.$totalRoman = convertToRoman(this.models.length);
      return model;
    });
  }

  onClickModel(index: number) {
    this.selectedModelIndex = index;
    this.selectedModel = this.models[this.selectedModelIndex];
    this.modelsService.notifyClick(index);
  }

  showNavigationItem(index: number) {
    if (this.selectedModelIndex === (this.models.length - 1)) {
      return index === 0;
    } else {
      return index === (this.selectedModelIndex + 1);
    }
  }
}

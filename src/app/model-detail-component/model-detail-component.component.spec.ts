import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailComponentComponent } from './model-detail-component.component';

describe('ModelDetailComponentComponent', () => {
  let component: ModelDetailComponentComponent;
  let fixture: ComponentFixture<ModelDetailComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelDetailComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Model } from './model.interface';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {

  private clicksubject = new Subject<number>();

  constructor() { }


  notifyClick(index: number) {
    this.clicksubject.next(index);
  }

  onClick$(): Observable<number> {
    return this.clicksubject.asObservable();
  }

  modelsList(): Model[] {
    return [
      {
        firstName: 'Belinda',
        lastName: 'Christelle',
        role: 'Model',
        instagram: 'belinda',
        country: 'Nigeria',
        state: 'Lagos',
        image: '../../assets/images/model-1.jpg',
        color: '#0e1d33'
      },
      {
        firstName: 'Milan',
        lastName: 'Christina',
        role: 'Supermodel',
        instagram: 'christina',
        country: 'USA',
        state: 'Los Angeles',
        image: '../../assets/images/model-2.jpg',
        color: '#543000'
      },
      {
        firstName: 'Rihanna',
        lastName: 'Niki',
        role: 'Supermodel',
        instagram: 'niki',
        country: 'Jamaica',
        state: 'Kingston',
        image: '../../assets/images/model-3.jpg',
        color: '#420422'
      },
      {
        firstName: 'Kate',
        lastName: 'Riley',
        role: 'Model',
        instagram: 'rileyk',
        country: 'Canada',
        state: 'Vancouver',
        image: '../../assets/images/model-4.jpg',
        color: '#230a22'
      }
    ];
  }
}

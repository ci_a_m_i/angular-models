import { Component, OnInit } from '@angular/core';
import { ModelsService } from './models.service';
import { Model } from './model.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  private selectedModelIndex = 0;
  private models: Model[];
  containerBackgroundStyle: { background: string } = { background: '#000' };

  constructor(private modelsService: ModelsService) {}

  ngOnInit() {
    this.models = this.modelsService.modelsList();
    this.modelsService.onClick$().subscribe(index => {
      this.selectedModelIndex = index;
      this. containerBackgroundStyle = { background: this.models[this.selectedModelIndex].color };
    });
  }
}

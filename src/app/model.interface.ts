export interface Model {
  firstName: string;
  lastName: string;
  role: string;
  instagram: string;
  country: string;
  state: string;
  image: string;
  color: string;
  $indexRoman?: string;
  $totalRoman?: string;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { ModelDetailComponentComponent } from './model-detail-component/model-detail-component.component';
import { ModelImageComponentComponent } from './model-image-component/model-image-component.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ModelDetailComponentComponent,
    ModelImageComponentComponent,
  ],
  imports: [
    BrowserModule, FlexLayoutModule,  BrowserAnimationsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

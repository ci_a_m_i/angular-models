import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelImageComponentComponent } from './model-image-component.component';

describe('ModelImageComponentComponent', () => {
  let component: ModelImageComponentComponent;
  let fixture: ComponentFixture<ModelImageComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelImageComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelImageComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

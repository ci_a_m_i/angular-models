import { Component, OnInit } from '@angular/core';
import { ModelsService } from '../models.service';
import { Model } from '../model.interface';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-model-image-component',
  templateUrl: './model-image-component.component.html',
  styleUrls: ['./model-image-component.component.sass'],
  animations: [
    trigger('changeBackgroundState', [
      state('backgroundRemoved', style({
          opacity: 0
        }
      )),
      state('newBackground', style({
        opacity: 1
      })),
      transition('void => newBackground', [
        animate(350),
      ]),
      transition('backgroundRemoved => newBackground', animate(350))
    ])
  ]
})
export class ModelImageComponentComponent implements OnInit {
  private selectedModelIndex = 0;
  private models: Model[];
  currentBackgroundState: string;

  backgroundImageStyle: { background: string } = { background: '#000' };

  constructor(private modelsService: ModelsService) {}

  ngOnInit() {
    this.models = this.modelsService.modelsList();
    this.backgroundImageStyle = { background: `url(${this.models[this.selectedModelIndex].image})` };
    this.currentBackgroundState = 'newBackground';
    this.modelsService.onClick$().subscribe(index => {
      this.currentBackgroundState = 'backgroundRemoved';
      this.selectedModelIndex = index;
      this.backgroundImageStyle = { background: `url(${this.models[this.selectedModelIndex].image})` };
      this.currentBackgroundState = 'newBackground';

    });
  }
}
